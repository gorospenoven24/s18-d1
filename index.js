/*objects:
	syntax:
		let objectName={
		keyA: valueA,
		keyB: valueB
		}

*/


let celphone ={
	name: 'nokia 3210',
	manufactureDate: 1999
}

console.log('Result from creating objects using initializer/literal notation');
console.log(celphone);
console.log(typeof celphone);


// Creating obejct suning constructor function
/*
create a reausable function to create several objecs that have the same data structre

 syntax:
 	function objectName(keyA,keyB){
	this.keyA = keyA;
	this.keyB =keyB;
 	}

 	"this". keyword allows to assign a new object properties by associating them with the values received from a construction function parameters
*/

function Laptop(name, manufactureDate){
	this.name = name;
	this.manufactureDate = manufactureDate;
}

let laptop = new Laptop('Lenovo', 2008);
console.log('Result from creating objects using initializer/literal notation');
console.log(laptop);

let mylaptop = new Laptop('macbook air', 2020);
console.log('Result from creating objects using initializer/literal notation');
console.log(mylaptop);

let ownlaptop = new Laptop('Asus', 2015);
console.log('Result from creating objects using initializer/literal notation');
console.log(ownlaptop);


// accessing object properties
// dot notation
console.log("Result from dot notation: " +mylaptop.name);


// square bracket notation
console.log("Result from dot notation: " +mylaptop['name']);


// access array objects

let arr = [laptop, mylaptop];
console.log(arr[0]['name']);
console.log(arr[0].name);


// Initializing/ adding/ deleting/ reassigning object properties

let car = {}

car.name = "Honda civic";
console.log("Result from adding properties using dot notation");
console.log(car);


car['Manufacture Date'] = 2019;
console.log(car['Manufacture Date']);
console.log(car);




// Deleting Object
delete car['Manufacture Date'];
console.log("Result from Deleting properties");
console.log(car);



// Reaasigning object properties
car.name = 'E46 BMW M3 GTR'
console.log("Result from Reasigning properties");
console.log(car);

// Obejct Method
// Method is a function which is a property of an object


//  person is an object--------------------------------------
let person ={
	name: 'John',
	talk: function (){
		console.log('Hello my name is ' + this.name)
	}
}
console.log(person);
console.log('Result from the object methods');
person.talk();


// adding method to obects
person.walk = function(){
	console.log(this.name + ' walk 25 steps forward');
}
person.walk();


// Add a run method to the person obejct
person.run = function(){
	console.log(this.name + ' run 25Km per hour');
}
person.run();


// creating  reausable functions
let friend = {
	firstName: 'Joe',
	lastName: ' Smith',
	address: {
		city: 'Uastin',
		country: 'Texas',
	},
	emails:['joe@gmail.com', 'joesmith@gmail.xyz'],
	introdue: function(){
		console.log('Hello my name is ' + this.firstName + this.lastName);
	}
}
friend.introdue();
console.log(friend.address);
console.log(friend.emails[1]);


// Real world aplication of obejct
/*scenario
	1.Create a game that would have several  pokemon interact with eah other
	2. Every pokemon would have the same set of stats, properties, and functions*/

let myPokemon = {
	name: 'Pikachu',
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(){
		console.log('This pokemon tackled target pokemon');
		console.log('taret pokemon health is now reduce to _targetPokemonhealth');
	},
	faint:function() {
		console.log("Pokeon fainted");
	}
};
console.log(myPokemon);


// pokemon object instructor
function Pokemon(name, level){
	// properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attact = level;

	// methods 
	this.tackle = function(target){
		console.log(this.name + 'tackled' + target.name);
		console.log('taret pokemon health is now reduce to _targetPokemonhealth');
	};
	this.faint = function(){
		console.log(this.name + 'fainted');
	}
}

// Creates new instances or instatiate or add of the pokemon object each with their unique properties

let pikachu = new Pokemon("Pikachu", 16);
let rattata = new Pokemon("Rattata", 8);


pikachu.tackle(rattata);
console.log(pikachu);
pikachu.tackle(rattata);

let momo = new Pokemon("Momo", 10);
momo.tackle(pikachu);




